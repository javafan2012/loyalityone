from django.shortcuts import render
from django.http import HttpResponse
import itertools

# method that takes a word and return
def anagrams(word):
    #make an empty list
    results = []
    #get all the permutations of the input word
    for i in itertools.permutations(word):
        #save item to string
        new_word = ''.join(i)
        # if the word is not in the list add it to list, if it is in the list do nothing
        if new_word not in results:
            results.append(new_word)
    #sort the list
    results.sort()
    #add all the items to a string and return it
    results = ', '.join(map(str, results))
    return results


def AllCombination(request, word):
    #get the word and use anagrams method to get the list of anagrams
    anagramsResult = anagrams(word)

    html = "<html><body>Word: %s </br> Anagrams:  %s.</body></html>" % (word, anagramsResult)
    return HttpResponse(html)