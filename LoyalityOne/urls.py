from django.conf.urls import patterns, include, url
from django.contrib import admin
from anagrams.views import AllCombination


urlpatterns = patterns('',
    url(r'^anagrams/(.*)/$', AllCombination),
)


