#!/bin/sh

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 GIT-HASH" >&2
  echo "To use specific commit, please use git hash"
fi

VER=$1



date=`date +%F-%H-%M-%S`

cd /var/www/

mv loyalityone loyalityone_$date

git clone https://javafan2012@bitbucket.org/javafan2012/loyalityone.git

cd loyalityone

git reset --hard $VER

sudo service apache2 restart
